package dataAccessLayer;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	protected String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}

	protected String createSelectAllQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		return sb.toString();
	}

	public List<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectAllQuery();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery(type.getDeclaredFields()[0].getName());
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	protected List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();
		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public String createInsertQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append(type.getSimpleName());
		sb.append(" (");
		Field[] fields = type.getDeclaredFields();
		for (int i = 1; i < fields.length; i++) {
			sb.append(fields[i].getName());
			if (i < fields.length - 1)
				sb.append(", ");
			else
				sb.append(") ");
		}
		sb.append("VALUES (");

		for (int i = 1; i < fields.length; i++) {

			if (i < fields.length - 1)
				sb.append("?, ");
			else
				sb.append("?)");
		}
		return sb.toString();
	}

	public T insertQuery(T t) {
		Connection connection = null;
		PreparedStatement statement = null;
		Field[] fields = type.getDeclaredFields();
		ResultSet k=null;
		T q=null;
		try {
			String sb = createInsertQuery();
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(sb);
			for (int i = 1; i < fields.length; i++) {
				fields[i].setAccessible(true);
				statement.setObject(i, fields[i].get(t));
			}
			statement.executeUpdate(statement.toString().split(":",2)[1], Statement.RETURN_GENERATED_KEYS);
			k=statement.getGeneratedKeys();
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:insertQuery " + e.getMessage());
			e.printStackTrace();
		} finally {
			
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return q;
	}

	public T updateQuery(int index, T t) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append(type.getSimpleName());
		sb.append(" SET ");
		Connection connection = null;
		PreparedStatement statement = null;
		Field[] fields = type.getDeclaredFields();
		try {
			for (int i = 1; i < fields.length; i++) {
				sb.append(fields[i].getName() + "=?");
				if (i < fields.length - 1)
					sb.append(", ");
				else
					sb.append(" WHERE " + fields[0].getName() + "=?");
			}
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(sb.toString());
			for (int i = 1; i < fields.length; i++) {
				fields[i].setAccessible(true);
				statement.setObject(i, fields[i].get(t));
			}
			statement.setObject(fields.length, index);
			statement.executeUpdate();
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:updateQuery " + e.getMessage());
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return t;
	}

	public int deleteQuery(int id) {
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE ");
		Connection connection = null;
		PreparedStatement statement = null;
		Field[] fields = type.getDeclaredFields();
		try {
			sb.append(fields[0].getName() + "=?");
			connection = ConnectionFactory.getConnection();
			try {
				statement = connection.prepareStatement(sb.toString());
				statement.setInt(1, id);
				statement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} finally {
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return id;
	}

	public void updateTable(JTable jt, List<T> t) {
			List<String> columns = new ArrayList<String>();
			List<String[]> values = new ArrayList<String[]>();
			Field[] fields = type.getDeclaredFields();
			int i, j, k;
			for (i = 0; i < fields.length; i++) {
				fields[i].setAccessible(true);
				columns.add(fields[i].getName());
			}
			for (j = 0; j < t.size(); j++) {
				String[] str = new String[i];
				for (k = 0; k < fields.length; k++) {
					fields[k].setAccessible(true);
					Object aux;
					try {
						aux = fields[k].get(t.get(j));
						str[k] = "" + aux;
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
				}
				values.add(str);
			}
			TableModel tableModel = new DefaultTableModel(values.toArray(new Object[][] {}), columns.toArray());
			jt.setModel(tableModel);
	}
}