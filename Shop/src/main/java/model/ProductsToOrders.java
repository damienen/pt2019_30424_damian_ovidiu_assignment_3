package model;

public class ProductsToOrders {
	private int idProductsToOrders;
	private int idProductA;
	private int idOrderA;
	private int quantity;
	public ProductsToOrders() {
		super();
		this.idOrderA=0;
		this.idProductA=0;
		this.idProductsToOrders=0;
		this.quantity=0;
	}
	public ProductsToOrders(int idProductsToOrders, int idProductA, int idOrderA, int quantity) {
		super();
		this.idProductsToOrders = idProductsToOrders;
		this.idProductA = idProductA;
		this.idOrderA = idOrderA;
		this.quantity = quantity;
	}
}
