package model;

public class Clients {
	private int idClients;
	private String nameClients;
	private String mailClients;
	private String addressClients;
	private String phoneClients;

	public Clients() {
		super();
		this.idClients=0;
		this.nameClients="";
		this.mailClients="";
		this.addressClients="";
		this.phoneClients="";
	}

	public Clients(int idClients, String nameClients, String mailClients, String addressClients, String phoneClients) {
		super();
		this.idClients = idClients;
		this.nameClients = nameClients;
		this.mailClients = mailClients;
		this.addressClients = addressClients;
		this.phoneClients = phoneClients;
	}

	@Override
	public String toString() {
		return getNameClients() + " " + getMailClients() + " " + getAddressClients() + " " + getPhoneClients();
	}

	public int getIdClients() {
		return idClients;
	}

	public void setIdClients(int idClients) {
		this.idClients = idClients;
	}

	public String getNameClients() {
		return nameClients;
	}

	public void setNameClients(String nameClients) {
		this.nameClients = nameClients;
	}

	public String getMailClients() {
		return mailClients;
	}

	public void setMailClients(String mailClients) {
		this.mailClients = mailClients;
	}

	public String getAddressClients() {
		return addressClients;
	}

	public void setAddressClients(String addressClients) {
		this.addressClients = addressClients;
	}

	public String getPhoneClients() {
		return phoneClients;
	}


}
