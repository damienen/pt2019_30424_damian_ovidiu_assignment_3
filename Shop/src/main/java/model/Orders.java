package model;

import java.sql.Timestamp;
import java.util.*;


public class Orders {
	private int idOrders;
	private Timestamp timeOrders;
	private int idClient;
	
	public Orders() {
		super();
		this.idOrders=0;
		this.timeOrders=new Timestamp(0);
		this.idClient=0;
	}

	public Orders(int idOrders, Timestamp timeOrders, int idClient) {
		super();
		this.idOrders = idOrders;
		this.timeOrders = timeOrders;
		this.idClient = idClient;
	}
	
	public int getIdOrders() {
		return idOrders;
	}

	public int getIdClient() {
		return idClient;
	}

}
