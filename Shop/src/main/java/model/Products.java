package model;

public class Products {
	private int idProducts;
	private String nameProducts;
	private int stockProducts;
	private String descriptionProducts;
	private float priceProducts;
	public Products() {
		super();
		this.idProducts=0;
		this.nameProducts="";
		this.stockProducts=0;
		this.descriptionProducts="";
		this.priceProducts=0;
	}
	public Products(int idProducts, String nameProducts, int stockProducts, String descriptionProducts,
			float priceProducts) {
		super();
		this.idProducts = idProducts;
		this.nameProducts = nameProducts;
		this.stockProducts = stockProducts;
		this.descriptionProducts = descriptionProducts;
		this.priceProducts = priceProducts;
	}

	@Override
	public String toString() {
		return getNameProducts() + " " + getStockProducts() + " " + getDescriptionProducts() + " " + getPriceProducts();
	}

	public int getIdProducts() {
		return idProducts;
	}

	public void setIdProducts(int idProducts) {
		this.idProducts = idProducts;
	}

	public String getNameProducts() {
		return nameProducts;
	}

	public void setNameProducts(String nameProducts) {
		this.nameProducts = nameProducts;
	}

	public int getStockProducts() {
		return stockProducts;
	}

	public void setStockProducts(int stockProducts) {
		this.stockProducts = stockProducts;
	}

	public String getDescriptionProducts() {
		return descriptionProducts;
	}

	public void setDescriptionProducts(String descriptionProducts) {
		this.descriptionProducts = descriptionProducts;
	}

	public float getPriceProducts() {
		return priceProducts;
	}

	public void setPriceProducts(float priceProducts) {
		this.priceProducts = priceProducts;
	}

}
