package presentation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.*;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

import businessLayer.*;
import dataAccessLayer.*;
import model.*;

public class ProductsFrame {
	public static void updateTable(JTable j) {
		ProductsDAO cd = new ProductsDAO();
		List<Products> l = cd.findAll();
		cd.updateTable(j, l);
	}

	public static boolean isIdInTable(JTable j, int id) {
		for (int i = 0; i < j.getRowCount(); i++) {
			if (Integer.parseInt(j.getValueAt(i, 0).toString()) == id)
				return true;
		}
		return false;
	}

	public static JComboBox createCombo() {
		ClientsDAO cd = new ClientsDAO();
		List<Clients> l = cd.findAll();
		String[] str = new String[l.size()];
		int i = 0;
		for (Clients c : l) {
			str[i] = c.getIdClients() + ":" + c.getNameClients();
			i++;
		}
		JComboBox jc = new JComboBox(str);
		return jc;
	}

	private JTable cartTable = new JTable();
	private JTable productsTable = new JTable();
	private JButton addProduct = new JButton("Add product");
	private JButton editProduct = new JButton("Edit product");
	private JButton deleteProduct = new JButton("Delete product");
	private JTextField nameField = new JTextField("");
	private JTextField descriptionField = new JTextField("");
	private JTextField stockField = new JTextField("");
	private JTextField priceField = new JTextField("");
	private JButton refresh = new JButton("Refresh table");
	private JButton addToCart = new JButton("Add to cart");
	private JSpinner quantity = new JSpinner();
	private JButton buy = new JButton("Buy");
	private JButton deletePCart = new JButton("Remove from cart");
	private boolean isOpen=false;
	
	public boolean isOpen() {
		return isOpen;
	}

	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}

	public ProductsFrame() {
		isOpen=true;
		final ProductsDAO cd = new ProductsDAO();
		updateTable(productsTable);
		final JScrollPane scrollPane = new JScrollPane(productsTable);
		final JFrame frame = new JFrame("Product window");
		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(scrollPane, BorderLayout.WEST);
		frame.getContentPane().add(panel);
		JPanel eastPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridheight = 1;
		c.gridwidth = 1;
		c.gridy = 0;
		c.gridx = 1;
		eastPanel.add(quantity, c);
		c.gridy = 1;
		c.gridx = 1;
		addToCart.setPreferredSize(new Dimension(120, 30));
		eastPanel.add(addToCart, c);
		nameField.setPreferredSize(new Dimension(150, 20));
		c.gridy = 2;
		c.gridx = 0;
		eastPanel.add(new JLabel("Name:"), c);
		c.gridx = 1;
		eastPanel.add(nameField, c);
		stockField.setPreferredSize(new Dimension(150, 20));
		c.gridy = 3;
		c.gridx = 0;
		eastPanel.add(new JLabel("Stock:"), c);
		c.gridx = 1;
		eastPanel.add(stockField, c);
		descriptionField.setPreferredSize(new Dimension(150, 20));
		c.gridy = 4;
		c.gridx = 0;
		eastPanel.add(new JLabel("Description:"), c);
		c.gridx = 1;
		eastPanel.add(descriptionField, c);
		priceField.setPreferredSize(new Dimension(150, 20));
		c.gridy = 5;
		c.gridx = 0;
		eastPanel.add(new JLabel("Price:"), c);
		c.gridx = 1;
		eastPanel.add(priceField, c);
		addProduct.setPreferredSize(new Dimension(120, 30));
		editProduct.setPreferredSize(new Dimension(120, 30));
		deleteProduct.setPreferredSize(new Dimension(120, 30));
		refresh.setPreferredSize(new Dimension(120, 30));
		c.gridy = 6;
		eastPanel.add(addProduct, c);
		c.gridy = 7;
		eastPanel.add(editProduct, c);
		c.gridy = 8;
		eastPanel.add(deleteProduct, c);
		c.gridy = 9;
		eastPanel.add(refresh, c);
		panel.add(eastPanel, BorderLayout.EAST);
		refresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				productsTable.clearSelection();
				updateTable(productsTable);
				scrollPane.revalidate();
				scrollPane.repaint();
				panel.revalidate();
				panel.repaint();
			}
		});
		frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                isOpen=false;
                e.getWindow().dispose();
            }
        });
		productsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent event) {
				if (productsTable.getSelectedRow() > -1) {
					nameField.setText(productsTable.getValueAt(productsTable.getSelectedRow(), 1).toString());
					stockField.setText(productsTable.getValueAt(productsTable.getSelectedRow(), 2).toString());
					descriptionField.setText(productsTable.getValueAt(productsTable.getSelectedRow(), 3).toString());
					priceField.setText(productsTable.getValueAt(productsTable.getSelectedRow(), 4).toString());
				}
			}
		});
		addProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Products t = new Products(1, nameField.getText(), Integer.parseInt(stockField.getText()),
							descriptionField.getText(), Float.parseFloat(priceField.getText()));
					ProductsValidator c = new ProductsValidator();
					c.validate(t);
					cd.insertQuery(t);
					updateTable(productsTable);
					scrollPane.revalidate();
					scrollPane.repaint();
					panel.revalidate();
					panel.repaint();
				} catch (IllegalArgumentException ee) {
					JOptionPane.showMessageDialog(null, ee.getMessage(), "Error: Illegal argument!",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		editProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Products t = new Products(1, nameField.getText(), Integer.parseInt(stockField.getText()),
						descriptionField.getText(), Float.parseFloat(priceField.getText()));
				ProductsValidator c = new ProductsValidator();
				try {
					c.validate(t);
					int i = productsTable.getSelectedRow();
					if (i > -1) {
						int index = Integer.parseInt(
								productsTable.getModel().getValueAt(productsTable.getSelectedRow(), 0).toString());
						cd.updateQuery(index, t);
						productsTable.clearSelection();
						updateTable(productsTable);
						scrollPane.revalidate();
						scrollPane.repaint();
						panel.revalidate();
						panel.repaint();
					}
				} catch (IllegalArgumentException ee) {
					JOptionPane.showMessageDialog(null, ee.getMessage(), "Error: Illegal argument!",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		deleteProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = productsTable.getSelectedRow();
				if (i > -1) {
					int index = Integer.parseInt(productsTable.getModel().getValueAt(i, 0).toString());
					cd.deleteQuery(index);
					productsTable.clearSelection();
					updateTable(productsTable);
					scrollPane.revalidate();
					scrollPane.repaint();
					panel.revalidate();
					panel.repaint();
				}
			}
		});

		final List<Products> cartProducts = new ArrayList<Products>();
		final JPanel southPanel = new JPanel(new GridBagLayout());
		final JScrollPane southScrollPane = new JScrollPane(cartTable);
		c.gridy = 0;
		c.gridx = 1;
		final JComboBox jc = createCombo();
		southPanel.add(jc, c);
		c.gridy = 1;
		c.gridx = 1;
		southPanel.add(new JLabel("Cart:"), c);
		cd.updateTable(cartTable, cartProducts);
		c.gridy = 2;
		c.gridx = 1;
		southPanel.add(southScrollPane, c);
		panel.add(southPanel, BorderLayout.SOUTH);
		c.gridy = 2;
		c.gridx = 2;
		southPanel.add(deletePCart, c);
		c.gridx = 3;
		southPanel.add(buy, c);
		addToCart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = productsTable.getSelectedRow();
				if (i > -1) {
					int id = Integer.parseInt(productsTable.getModel().getValueAt(i, 0).toString());
					int stock = Integer.parseInt(productsTable.getModel().getValueAt(i, 2).toString());
					if (Integer.parseInt(quantity.getValue().toString()) <= 0)
						JOptionPane.showMessageDialog(null, "Can't order negative or null quantity",
								"Error: Illegal argument!", JOptionPane.ERROR_MESSAGE);
					else if (Integer.parseInt(quantity.getValue().toString()) > stock) {
						JOptionPane.showMessageDialog(null, "Can't order a quantity over the available stock",
								"Error: Illegal argument!", JOptionPane.ERROR_MESSAGE);
					} else {
						if (!isIdInTable(cartTable, id)) {
							Products t = cd.findById(id);
							t.setStockProducts(Integer.parseInt(quantity.getValue().toString()));
							t.setPriceProducts(Float.parseFloat(productsTable.getModel().getValueAt(i, 4).toString())
									* Integer.parseInt(quantity.getValue().toString()));
							cartProducts.add(t);
							cd.updateTable(cartTable, cartProducts);
						} else {
							JOptionPane.showMessageDialog(null, "Can't order the same product twice in the same order",
									"Error: Illegal argument!", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		});
		deletePCart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = cartTable.getSelectedRow();
				if (i > -1) {
					cartProducts.remove(i);
					cartTable.clearSelection();
					cd.updateTable(cartTable, cartProducts);
					southScrollPane.revalidate();
					southScrollPane.repaint();
					southPanel.revalidate();
					southPanel.repaint();
				}
			}
		});
		buy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (cartTable.getRowCount() > 0) {
					String str="";
					String sid = String.valueOf(jc.getSelectedItem());
					int id = Integer.parseInt(sid.split(":")[0]);
					OrdersDAO d = new OrdersDAO();
					Date date = new Date();
					Timestamp ts = new Timestamp(date.getTime());
					Orders o = new Orders(1, ts, id);
					d.insertQuery(o);
					List<Orders> or = d.findAll();
					o = or.get(or.size() - 1);
					ClientsDAO c=new ClientsDAO();
					try {
						PrintWriter writer=new PrintWriter("ShopInvoice #"+o.getIdOrders()+".txt","UTF-8");
						writer.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
						writer.println("Ovidiu's shop\nNoName street, number INF");
						Clients cc=c.findById(o.getIdClient());
						writer.println("Client data:\n"+cc.toString());
						writer.println(ts.toString());
						writer.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
						
						for (int i = 0; i < cartTable.getRowCount(); i++) {
							int productId = Integer.parseInt(cartTable.getValueAt(i, 0).toString());
							int quantity = Integer.parseInt(cartTable.getValueAt(i, 2).toString());
							float price = Float.parseFloat(cartTable.getValueAt(i, 4).toString());
							ProductsToOrdersDAO pto = new ProductsToOrdersDAO();
							ProductsToOrders p = new ProductsToOrders(1, productId, o.getIdOrders(), quantity);
							pto.insertQuery(p);
							ProductsDAO prd = new ProductsDAO();
							Products pr = prd.findById(productId);
							pr.setStockProducts(pr.getStockProducts() - quantity);
							prd.updateQuery(productId, pr);
							writer.println(quantity+"x "+pr.getNameProducts()+":"+pr.getIdProducts()+"-> "+price+"$\n");
						}
						writer.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
						writer.println("Thank you for shopping with us!");
						writer.close();
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (UnsupportedEncodingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					updateTable(productsTable);
					DefaultTableModel model = (DefaultTableModel) cartTable.getModel();
					model.setRowCount(0);
				} else {
					JOptionPane.showMessageDialog(null, "Can't place an empty order", "Error: Illegal argument!",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		frame.setSize(frame.getPreferredSize());
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setResizable(false);
		frame.setVisible(true);
	}
}
