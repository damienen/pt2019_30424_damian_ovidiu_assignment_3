package presentation;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class GeneralFrame {
	private JButton clientsButton=new JButton("Clients window");
	private JButton productsButton=new JButton("Products and order window");
	private JFrame frame=new JFrame("Shop");
	private JPanel panel=new JPanel();
	private ClientFrame cf;
	private ProductsFrame pf;
	public GeneralFrame() {
		clientsButton.setSize(clientsButton.getPreferredSize());
		productsButton.setSize(productsButton.getPreferredSize());
		productsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(cf==null||!cf.isOpen())
					pf=new ProductsFrame();
				else
					JOptionPane.showMessageDialog(null, "Only one window can be open at a time!", "Error!", JOptionPane.ERROR_MESSAGE);
			}
		});
		clientsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(pf==null||!pf.isOpen())
					cf=new ClientFrame();
				else
					JOptionPane.showMessageDialog(null, "Only one window can be open at a time!", "Error!", JOptionPane.ERROR_MESSAGE);
			}
		});
		panel.add(clientsButton);
		panel.add(productsButton);
		frame.getContentPane().add(panel);
		frame.setPreferredSize(new Dimension(200,100));
		frame.setSize(frame.getPreferredSize());
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setVisible(true);
	}
}
