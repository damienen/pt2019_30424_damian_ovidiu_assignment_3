package presentation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.*;

import businessLayer.ClientsValidator;
import dataAccessLayer.ClientsDAO;
import dataAccessLayer.ConnectionFactory;
import dataAccessLayer.ProductsDAO;
import model.Clients;
import model.Products;

public class ClientFrame {
	public static void updateTable(JTable j) {
		ClientsDAO cd=new ClientsDAO();
		List<Clients> l=cd.findAll();
		cd.updateTable(j, l);
	}
	
	private boolean isOpen=false;
	private JTable clientsTable=new JTable();
	private JButton addClient=new JButton("Add client");
	private JButton editClient=new JButton("Edit client");
	private JButton deleteClient=new JButton("Delete client");
	private JTextField nameField=new JTextField("");
	private JTextField addressField=new JTextField("");
	private JTextField mailField=new JTextField("");
	private JTextField phoneField=new JTextField("");
	private JButton refresh=new JButton("Refresh table");
	
	public boolean isOpen() {
		return isOpen;
	}

	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}

	public ClientFrame() {
		isOpen=true;
		final ClientsDAO cd=new ClientsDAO();
		updateTable(clientsTable);
		final JScrollPane scrollPane=new JScrollPane(clientsTable);
		final JFrame frame = new JFrame("Client Window");
		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(scrollPane,BorderLayout.WEST);
		frame.getContentPane().add(panel);
		JPanel eastPanel=new JPanel(new GridBagLayout());
		GridBagConstraints c=new GridBagConstraints();
		c.gridheight=1;c.gridwidth=1;
		nameField.setPreferredSize(new Dimension(150,20));
		c.gridy=0;c.gridx=0;eastPanel.add(new JLabel("Name:"),c);c.gridx=1;eastPanel.add(nameField,c);
		addressField.setPreferredSize(new Dimension(150,20));
		c.gridy=1;c.gridx=0;eastPanel.add(new JLabel("Address:"),c);c.gridx=1;eastPanel.add(addressField,c);
		mailField.setPreferredSize(new Dimension(150,20));
		c.gridy=2;c.gridx=0;eastPanel.add(new JLabel("Mail:"),c);c.gridx=1;eastPanel.add(mailField,c);
		phoneField.setPreferredSize(new Dimension(150,20));
		c.gridy=3;c.gridx=0;eastPanel.add(new JLabel("Phone:"),c);c.gridx=1;eastPanel.add(phoneField,c);
		addClient.setPreferredSize(new Dimension(120,30));
		editClient.setPreferredSize(new Dimension(120,30));
		deleteClient.setPreferredSize(new Dimension(120,30));
		refresh.setPreferredSize(new Dimension(120,30));
		c.gridy=5;eastPanel.add(addClient,c);c.gridy=6;eastPanel.add(editClient,c);c.gridy=7;eastPanel.add(deleteClient,c);c.gridy=8;eastPanel.add(refresh,c);
		panel.add(eastPanel,BorderLayout.EAST);
		addClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Clients t=new Clients(1,nameField.getText(),mailField.getText(),addressField.getText(),phoneField.getText());
				ClientsValidator c=new ClientsValidator();
				
				try{c.validate(t);
				cd.insertQuery(t);
				updateTable(clientsTable);
				scrollPane.revalidate();
				scrollPane.repaint();
				panel.revalidate();
				panel.repaint();
				}catch(IllegalArgumentException ee) {
					JOptionPane.showMessageDialog(null, ee.getMessage(), "Error: Illegal argument!", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		editClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Clients t=new Clients(1,nameField.getText(),mailField.getText(),addressField.getText(),phoneField.getText());
				ClientsValidator c=new ClientsValidator();
				
				try{c.validate(t);
				int index=Integer.parseInt(clientsTable.getModel().getValueAt(clientsTable.getSelectedRow(), 0).toString());
				cd.updateQuery(index,t);
				clientsTable.clearSelection();
				updateTable(clientsTable);
				scrollPane.revalidate();
				scrollPane.repaint();
				panel.revalidate();
				panel.repaint();
				}catch(IllegalArgumentException ee) {
					JOptionPane.showMessageDialog(null, ee.getMessage(), "Error: Illegal argument!", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		deleteClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index=Integer.parseInt(clientsTable.getModel().getValueAt(clientsTable.getSelectedRow(), 0).toString());
				cd.deleteQuery(index);
				clientsTable.clearSelection();
				updateTable(clientsTable);
				scrollPane.revalidate();
				scrollPane.repaint();
				panel.revalidate();
				panel.repaint();
			}
		});
		frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                isOpen=false;
                e.getWindow().dispose();
            }
        });
		refresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clientsTable.clearSelection();
				updateTable(clientsTable);
				scrollPane.revalidate();
				scrollPane.repaint();
				panel.revalidate();
				panel.repaint();
			}
		});
		clientsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	        	if(clientsTable.getSelectedRow()>-1) {
	        	nameField.setText(clientsTable.getValueAt(clientsTable.getSelectedRow(), 1).toString());
	        	mailField.setText(clientsTable.getValueAt(clientsTable.getSelectedRow(), 2).toString());
	        	addressField.setText(clientsTable.getValueAt(clientsTable.getSelectedRow(), 3).toString());
	        	phoneField.setText(clientsTable.getValueAt(clientsTable.getSelectedRow(), 4).toString());}
	        }
	    });
		frame.setPreferredSize(new Dimension(660, 480));
		frame.setSize(frame.getPreferredSize());
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setResizable(false);
		frame.setVisible(true);
	}
}
