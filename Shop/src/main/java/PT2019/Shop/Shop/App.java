package PT2019.Shop.Shop;

import javax.swing.*;
import presentation.*;

public class App 
{
    public static void main( String[] args )
    {	
    	SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new GeneralFrame();
			}
		});
    }
}
