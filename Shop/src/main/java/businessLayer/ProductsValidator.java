package businessLayer;

import java.util.*;

import dataAccessLayer.ProductsDAO;
import model.Clients;
import model.Products;

public class ProductsValidator {
	private List<Validator<Products>> validators;
	private ProductsDAO productDAO;
	public ProductsValidator() {
		validators=new ArrayList<Validator<Products>>();
		validators.add(new PriceValidator());
		validators.add(new StockValidator());
	}
	public void validate(Products t) {
		for(Validator<Products> v:validators) {
			v.validate(t);}
	}
}
