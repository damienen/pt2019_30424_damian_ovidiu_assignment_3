package businessLayer;

import java.util.regex.Pattern;

import model.Clients;

public class NumberValidator implements Validator<Clients>{
	private static final String NUMBER_PATTERN ="^(?:[+\\d].*\\d|\\d)$";
	public void validate(Clients t) {
		Pattern pattern = Pattern.compile(NUMBER_PATTERN);
		if (!pattern.matcher(t.getPhoneClients()).matches()) {
			throw new IllegalArgumentException("Number of client is not valid!");
		}
	}
}
