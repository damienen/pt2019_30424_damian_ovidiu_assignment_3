package businessLayer;

import java.util.*;

import javax.swing.JOptionPane;

import model.*;
import dataAccessLayer.*;
public class ClientsValidator {
	private List<Validator<Clients>> validators;
	private ClientsDAO clientDAO;
	public ClientsValidator() {
		validators=new ArrayList<Validator<Clients>>();
		validators.add(new NameValidator());
		validators.add(new EmailValidator());
		validators.add(new AddressValidator());
		validators.add(new NumberValidator());
		clientDAO=new ClientsDAO();
	}
	public Clients findClientById(int id) {
		Clients c=clientDAO.findById(id);
		if(c==null)
			throw new NoSuchElementException("The client with id =" + id + " was not found!");
		return c;
	}
	public void validate(Clients t) {
		for(Validator<Clients> v:validators) {
			v.validate(t);}
	}
}
