package businessLayer;

import java.util.regex.Pattern;

import model.Clients;

public class NameValidator implements Validator<Clients>{
	private static final String NAME_PATTERN ="^[\\p{L} .'-]+$";
	public void validate(Clients t) {
		Pattern pattern = Pattern.compile(NAME_PATTERN);
		if (!pattern.matcher(t.getNameClients()).matches()) {
			throw new IllegalArgumentException("Name of client is not valid!");
		}
	}
}
