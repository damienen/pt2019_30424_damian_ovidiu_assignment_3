package businessLayer;

import model.Products;

public class PriceValidator implements Validator<Products>{
	private static final int MIN_PRICE = 0;
	public void validate(Products t) {
		if(!(t.getPriceProducts()>MIN_PRICE))
			throw new IllegalArgumentException("The price is invalid");
	}
}
