package businessLayer;

import model.Clients;

public class AddressValidator implements Validator<Clients> {
	public void validate(Clients t) {
		if(t.getAddressClients().isEmpty())
			throw new IllegalArgumentException("Address of client is not valid!");
	}
}
