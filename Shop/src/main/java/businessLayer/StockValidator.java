package businessLayer;

import model.Products;

public class StockValidator implements Validator<Products>{
	private static final int MIN_STOCK = 0;
	public void validate(Products t) {
		if(!(t.getPriceProducts()>=MIN_STOCK))
			throw new IllegalArgumentException("The price is invalid");
	}
}
